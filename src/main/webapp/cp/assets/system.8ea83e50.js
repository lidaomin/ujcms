import{x as t}from"./index.35346a57.js";const s=async a=>(await t.get("/backend/core/site",{params:a})).data,c=async a=>(await t.get("/backend/core/attachment",{params:a})).data,n=async a=>(await t.post("/backend/core/attachment?_method=delete",a)).data,o=async a=>(await t.get("/backend/core/task",{params:a})).data,d=async a=>(await t.get(`/backend/core/task/${a}`)).data,r=async a=>(await t.post("/backend/core/task",a)).data,i=async a=>(await t.post("/backend/core/task?_method=put",a)).data,k=async a=>(await t.post("/backend/core/task?_method=delete",a)).data,y=async a=>(await t.get("/backend/core/process-definition",{params:a})).data,b=async a=>(await t.get(`/backend/core/process-instance/task/${a}`)).data;export{y as a,c as b,d as c,n as d,r as e,k as f,o as g,s as h,b as q,i as u};
