package com.ujcms.cms.core.domain;

import com.ujcms.cms.core.domain.base.ArticleFileBase;

import java.io.Serializable;

/**
 * 文章文件集 实体类
 *
 * @author PONY
 */
public class ArticleFile extends ArticleFileBase implements Serializable {
}