package com.ujcms.cms.core.domain;

import com.ujcms.cms.core.domain.base.ArticleImageBase;

import java.io.Serializable;

/**
 * 文章图片集 实体类
 *
 * @author PONY
 */
public class ArticleImage extends ArticleImageBase implements Serializable {
}