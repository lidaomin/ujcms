package com.ujcms.cms.core.domain;

import com.ujcms.cms.core.domain.base.UserExtBase;

import java.io.Serializable;

/**
 * 用户扩展数据 实体类
 *
 * @author PONY
 */
public class UserExt extends UserExtBase implements Serializable {
}