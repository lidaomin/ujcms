package com.ujcms.cms.core.domain.base;

import java.time.OffsetDateTime;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Length;
import org.springframework.lang.Nullable;

/**
 * This class was generated by MyBatis Generator.
 *
 * @author MyBatis Generator
 */
public class UserBase {
    /**
     * 数据库表名
     */
    public static final String TABLE_NAME = "user";

    /**
     * 用户ID
     */
    @NotNull
    private Integer id = 0;

    /**
     * 用户组ID
     */
    @NotNull
    private Integer groupId = 0;

    /**
     * 组织ID
     */
    @NotNull
    private Integer orgId = 0;

    /**
     * 用户名
     */
    @Length(max = 30)
    @NotNull
    private String username = "";

    /**
     * 密码
     */
    @Length(max = 64)
    @NotNull
    private String password = "0";

    /**
     * 密码混淆码
     */
    @Length(max = 32)
    @NotNull
    private String salt = "0";

    /**
     * 电子邮箱
     */
    @Length(max = 50)
    @Nullable
    private String email;

    /**
     * 手机号码
     */
    @Length(max = 50)
    @Nullable
    private String mobile;

    /**
     * 博客地址
     */
    @Length(max = 50)
    @Nullable
    private String alias;

    /**
     * 显示名
     */
    @Length(max = 50)
    @Nullable
    private String displayName;

    /**
     * 头像URL
     */
    @Length(max = 255)
    @Nullable
    private String avatar;

    /**
     * 密码修改时间
     */
    @NotNull
    private OffsetDateTime passwordModified = OffsetDateTime.now();

    /**
     * 等级
     */
    @NotNull
    private Short rank = 999;

    /**
     * 类型(1:系统管理员,2:安全管理员,3:审计管理员,4:常规管理员,5:前台会员)
     */
    @NotNull
    private Short type = 4;

    /**
     * 状态(0:正常,1:未激活,2:已锁定,3:已注销)
     */
    @NotNull
    private Short status = 0;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public Integer getOrgId() {
        return orgId;
    }

    public void setOrgId(Integer orgId) {
        this.orgId = orgId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    @Nullable
    public String getEmail() {
        return email;
    }

    public void setEmail(@Nullable String email) {
        this.email = email;
    }

    @Nullable
    public String getMobile() {
        return mobile;
    }

    public void setMobile(@Nullable String mobile) {
        this.mobile = mobile;
    }

    @Nullable
    public String getAlias() {
        return alias;
    }

    public void setAlias(@Nullable String alias) {
        this.alias = alias;
    }

    @Nullable
    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(@Nullable String displayName) {
        this.displayName = displayName;
    }

    @Nullable
    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(@Nullable String avatar) {
        this.avatar = avatar;
    }

    public OffsetDateTime getPasswordModified() {
        return passwordModified;
    }

    public void setPasswordModified(OffsetDateTime passwordModified) {
        this.passwordModified = passwordModified;
    }

    public Short getRank() {
        return rank;
    }

    public void setRank(Short rank) {
        this.rank = rank;
    }

    public Short getType() {
        return type;
    }

    public void setType(Short type) {
        this.type = type;
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }
}